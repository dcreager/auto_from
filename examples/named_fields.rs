use auto_from::From;

#[derive(Debug, From)]
enum Foo {
    Int(i32),
    Long { value: i64 },
}

fn main() {
    println!("foo_int => {:?}", Foo::from(24i32));
    println!("foo_long => {:?}", Foo::from(24i64));
}
