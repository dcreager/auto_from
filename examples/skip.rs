use auto_from::From;

#[derive(Debug, From)]
enum Foo {
    #[auto_from(skip)]
    SkippedInt(i32),
    Int(i32),
    Double(f64),
}

fn main() {
    println!("foo_double => {:?}", Foo::from(24.12f64));
    println!("foo_int => {:?}", Foo::from(24i32));
}
